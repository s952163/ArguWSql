﻿namespace ArguWSql

#if INTERACTIVE
#r @"..\packages\SQLProvider\lib\FSharp.Data.SqlProvider.dll"
#r @"..\packages\Npgsql\lib\net451\Npgsql.dll"
#endif

open System.IO
open FSharp.Data
open FSharp.Data.Sql

module Library =
    
    [<Literal>]
    let private csvFile = @"c:\tmp\testDeedle.csv"
    type private CsvTest = CsvProvider<csvFile>


    
    let printDebug x =
        let file = Path.Combine(fst x,snd x)  
        let check = file |> File.Exists
        
        if check then printfn "%A" (file + " exists. Good to go") 
        else printfn "%A" "Woot?"

    let readCsvFile (x:string) =
        let csv = CsvTest.Load x
        csv.Rows 
            |> Seq.take 10
            |> Seq.toArray
            |> printfn "%A"

module SqlDBConn =
    open System

    let [<Literal>] private dbVendor = Common.DatabaseProviderTypes.POSTGRESQL
    let [<Literal>] private connString = "Host=localhost;Database=BARZ;Username=postgres;Password=root"
    let [<Literal>] private connStringName = "DefaultConnectionString"
    let [<Literal>] private resPath = __SOURCE_DIRECTORY__ + @"\..\packages\Npgsql\lib\net451"
    let [<Literal>] private indivAmount = 1000
    let [<Literal>] private useOptTypes = false

    type private sql = SqlDataProvider<dbVendor, connString,"",resPath,indivAmount,useOptTypes>


    /// just get database context
    let private getDbx() =
        sql.GetDataContext()

    let private dbx = getDbx()
    let private table1 = dbx.Public.Jpnciw1Tbl
    let private someData = 
        query { for rows in table1 do  
                where (rows.Datadate >= DateTime(2017,3,1))
                select rows.Assetname 
                } |> Seq.take 10 |> Seq.toList
    
    let printOut () =
        someData |> Seq.iteri (fun i x -> printfn "%A" (i,x))

