﻿// Learn more about F# at http://fsharp.org
// See the 'F# Tutorial' project for more help.


open FSharp.Data
open Argu
open System
open ArguWSql
open System.IO

type Arguments =
    | Path of path:string
    | File of file:string
    | DB 
    | DryRun
with
    interface IArgParserTemplate with
        member s.Usage =
            match s with
            | Path _ -> "Specify a working directory."
            | File _ -> "Specify a file name."
            | DB -> "Connect to DB."
            | DryRun _ -> "Test run."

[<EntryPoint>]
let main argv = 
   
   let errorHandler = ProcessExiter(colorizer = function ErrorCode.HelpText -> None | _ -> Some ConsoleColor.Red)
   let parser = ArgumentParser.Create<Arguments>(programName = "ArguWSql.exe",errorHandler=errorHandler)
   let usage = parser.PrintUsage()
   let results = parser.ParseCommandLine argv
   let all = results.GetAllResults()

   let path = results.GetResult (<@ Path @>, defaultValue = @"c:\tmp")
   let file = results.GetResult (<@ File @>, defaultValue = "testDeedle.csv")
   let dryrun = results.Contains <@ DryRun @>
   let db = results.Contains <@ DB @> 
   
   match dryrun with 
   | true  -> Library.printDebug (path,file)
   | false when not db -> Library.readCsvFile <| Path.Combine(path,file)
   | false when db -> SqlDBConn.printOut()
   | _ -> ()


    


   //printfn "%A" usage
   0 // return an integer exit code
